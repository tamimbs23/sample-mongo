﻿using Amazon.Runtime.Internal.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson.IO;
using SampleMongo.Models;
using SampleMongo.Repositories;

namespace SampleMongo.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductRepository _productRepository;
        public ProductController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        [HttpGet]
        public ActionResult GetProducts()
        {
            var products = _productRepository.GetAll<Product>();

            return Ok(products);
        }

        [HttpGet]
        public ActionResult GetProduct(string id)
        {
            Product product = _productRepository.Get<Product>(id);

            return Ok(product);
        }

        [HttpGet]
        public ActionResult GetProductBySku(string sku)
        {
            var product = _productRepository.GetProductBySku<Product>(sku);

            return Ok(product);
        }

        [HttpPost]
        public ActionResult UpdateProductBySku(string sku, Product newProduct)
        {
            Product product = _productRepository.GetBySku<Product>(sku);
            newProduct.PrimaryId = product.PrimaryId;

            _productRepository.UpdateInfo<Product>(product.PrimaryId, newProduct);

            return Ok(newProduct);
        }

        [HttpPost]
        public RedirectToActionResult Create(Product record)
        {
            Product newProduct = _productRepository.Create<Product>(record);
            return RedirectToAction("GetProduct", new { id = newProduct.PrimaryId });
        }
    }
}
