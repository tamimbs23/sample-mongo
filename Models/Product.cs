﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace SampleMongo.Models
{
    public class Product
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string? PrimaryId { get; set; }
        public string? Name { get; set; }
        public string? Category { get; set; }
        public string? Sku { get; set; }
        public string? Description { get; set; }
        public double Price { get; set; }
    }
}
