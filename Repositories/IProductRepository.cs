﻿using MongoDB.Driver.Linq;
using SampleMongo.Models;

namespace SampleMongo.Repositories
{
    public interface IProductRepository
    {
        IList<T> GetAll<T>();
        T Get<T>(string Id);
        T GetBySku<T>(string sku);
        T Create<T>(T record);
        Product GetProductBySku<T>(string sku);
        void UpdateInfo<T>(string Id, T record);
    }
}
