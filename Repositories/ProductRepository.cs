﻿using MongoDB.Driver;
using MongoDB.Driver.Linq;
using SampleMongo.Models;

namespace SampleMongo.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private IMongoDatabase db;
        private readonly IConfiguration _configuration;

        public ProductRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            MongoClientSettings settings = MongoClientSettings
                .FromConnectionString(_configuration["ConnectionString"]);
            settings.LinqProvider = LinqProvider.V3;
            var client = new MongoClient(settings);
            db = client.GetDatabase(_configuration["ProductDBName"]);
            //_Collection = database.GetCollection<Student>(settings.StudentsCollectionName);
        }

        public IList<T> GetAll<T>()
        {
            string name = typeof(T).Name;
            var collectionName = $"{name}s";
            var collection = db.GetCollection<T>(name + "s");
            return collection.Find(_ => true).ToList();
        }

        public T Get<T>(string Id)
        {
            string name = typeof(T).Name;
            var collectionName = $"{name}s";
            var collection = db.GetCollection<T>(name + "s");
            var filter = Builders<T>.Filter.Eq("PrimaryId", Id);
            return collection.Find(filter).First();
        }

        public T GetBySku<T>(string sku)
        {
            string name = typeof(T).Name;
            var collection = db.GetCollection<T>(name + "s");
            var filter = Builders<T>.Filter.Eq("Sku", sku);
            return collection.Find(filter).First();
        }

        public Product GetProductBySku<T>(string sku)
        {
            string name = typeof(T).Name;
            var collection = db.GetCollection<Product>(name + "s");
            var p = collection.AsQueryable().Where(p => p.Sku == sku).FirstOrDefault();

            return p;
        }


        public T Create<T>(T record)
        {
            var collection = db.GetCollection<T>(record.GetType().Name + "s");

            collection.InsertOne(record);
            return record;
        }

        public void UpdateInfo<T>(string Id, T record)
        {
            string name = typeof(T).Name;
            var collection = db.GetCollection<T>(name + "s");
            var filter = Builders<T>.Filter.Eq("PrimaryId", Id);
            collection.ReplaceOne(filter, record, new ReplaceOptions() { IsUpsert = false });
        }
    }
}
